package antflip.home


import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import antflip.HomeActivity
import antflip.R
import antflip.callBacks.Request
import antflip.callBacks.ResponseListener
import antflip.retrofit.RetrofitClient
import antflip.retrofit.RetrofitRequest
import antflip.utility.PreferenceManager
import antflip.utility.Util
import antflip.utility.Util.Companion.createCabMarker
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import okhttp3.Headers
import java.lang.Double.parseDouble


/**
 * A simple [Fragment] subclass.
 *
 */
class HomeFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
    GoogleMap.OnMapLongClickListener {

    private val TAG = "HomeFragment : " + javaClass.simpleName

    private var mMap: GoogleMap? = null
    private var mapFragment: SupportMapFragment? = null
    //    private var myMarker: Marker? = null
    var btnSearch: EditText? = null
    var btnSearchOne: EditText? = null
    var etLocationOne: EditText? = null
    var btnCabs: Button? = null
    var btnrev: TextView? = null
    var etLocation: EditText? = null
    lateinit var dummyMarker: ImageView
    var isLocationDown: Boolean = true
    lateinit var progress: ProgressBar

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false) as View

        mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment!!.getMapAsync(this)
        btnSearch = view.findViewById(R.id.btnSearch) as EditText
        btnSearchOne = view.findViewById(R.id.btnSearchOne) as EditText
        etLocationOne = view.findViewById(R.id.etLocationOne) as EditText
        btnCabs = view.findViewById(R.id.btnCabs) as Button
        btnrev = view.findViewById(R.id.btnrev) as TextView
        etLocation = view.findViewById(R.id.etLocation) as EditText
        dummyMarker = view.findViewById(R.id.dummyMarker) as ImageView
        progress = view.findViewById(R.id.progress) as ProgressBar
        btnSearch!!.setOnClickListener {
            (activity as HomeActivity).placeSearch()
        }
        etLocation!!.setOnClickListener {
            (activity as HomeActivity).placeSearch()
        }
        btnSearchOne!!.setOnClickListener {
            (activity as HomeActivity).placeSearch()
        }
        btnCabs!!.setOnClickListener {
            progress.visibility=View.VISIBLE
            (activity as (HomeActivity)).getLocation()
        }
        progress.visibility = VISIBLE
        setVisibility(true)
        btnrev!!.setOnClickListener {


            isLocationDown = !isLocationDown
            setVisibility(isLocationDown)

        }
        dummyMarker.setImageBitmap(activity?.let { Util.createCustomMarker(it, "#d2d2d2", "test") })
        dummyMarker.visibility = View.GONE

        etLocation!!.setOnTouchListener(OnTouchListener { v, event ->
            val DRAWABLE_LEFT = 0
            val DRAWABLE_TOP = 1
            val DRAWABLE_RIGHT = 2
            val DRAWABLE_BOTTOM = 3

            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= etLocation!!.getRight() - etLocation!!.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width()) {
                    // your action here
                    myLocationClicked = true
                    var pref: PreferenceManager
                    pref = PreferenceManager(activity)
                    val ownLoc = (LatLng(pref.locationLat.toDouble(), pref.locationLon.toDouble()))
                    setOwnMarker(ownLoc)
                    etLocation!!.setText(
                        Util.getAddress(
                            activity,
                            ownLoc.latitude,
                            ownLoc.longitude
                        )
                    )
                    btnSearchOne!!.setText(
                        Util.getAddress(
                            activity,
                            ownLoc.latitude,
                            ownLoc.longitude
                        )
                    )

                    return@OnTouchListener true
                }
            }
            false
        })
        btnSearchOne!!.setOnTouchListener(OnTouchListener { v, event ->
            val DRAWABLE_LEFT = 0
            val DRAWABLE_TOP = 1
            val DRAWABLE_RIGHT = 2
            val DRAWABLE_BOTTOM = 3

            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= btnSearchOne!!.getRight() - btnSearchOne!!.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width()) {
                    // your action here
                    myLocationClicked = true
                    var pref: PreferenceManager
                    pref = PreferenceManager(activity)
                    val ownLoc = (LatLng(pref.locationLat.toDouble(), pref.locationLon.toDouble()))
                    setOwnMarker(ownLoc)
                    etLocation!!.setText(
                        Util.getAddress(
                            activity,
                            ownLoc.latitude,
                            ownLoc.longitude
                        )
                    )
                    btnSearchOne!!.setText(
                        Util.getAddress(
                            activity,
                            ownLoc.latitude,
                            ownLoc.longitude
                        )
                    )
                    return@OnTouchListener true
                }
            }
            false
        })
        return view;
    }

    var myLocationClicked = false
    private fun setVisibility(isLocationDown: Boolean) {
        if (isLocationDown) {
            btnSearch!!.visibility = View.VISIBLE
            btnSearchOne!!.visibility = View.INVISIBLE
            etLocation!!.visibility = View.VISIBLE
            etLocationOne!!.visibility = View.INVISIBLE
        } else {
            btnSearch!!.visibility = View.INVISIBLE
            btnSearchOne!!.visibility = View.VISIBLE
            etLocation!!.visibility = View.INVISIBLE
            etLocationOne!!.visibility = View.VISIBLE
        }
    }

    var locReceived = false
    fun onLocationReceived(lat: String, log: String, isOwnLoc: Boolean) {

        if (!locReceived) {

            locReceived = true
            getCabDetails()
        }


        progress.visibility = GONE
        if (mapFragment != null || mMap != null) {
            Util.showSnackBar(mapFragment?.getView()!!, "Location Received")

            if (isOwnLoc) {

//                mMap?.clear()
                val ownLoc = LatLng(parseDouble(lat), parseDouble(log))
//                mMap?.moveCamera(CameraUpdateFactory.newLatLng(ownLoc))
//                etLocation!!.setText(Util.getAddress(activity, ownLoc.latitude, ownLoc.longitude))
//                setOwnMarker(ownLoc)
//                (activity as HomeActivity).dummyImageVissibility(true)
                mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(ownLoc, 15f))
                mMap!!.setOnCameraIdleListener {
                    dummyMarker.visibility = View.GONE
                    mMap?.clear()
                    cabDetail?.let { setcabsInMap(it) }
                    val midLatLng = mMap!!.getCameraPosition().target
                    mMap?.addMarker(
                        MarkerOptions().position(midLatLng).icon(
                            BitmapDescriptorFactory.fromBitmap(
                                activity?.let { Util.createCustomMarker(it, "#d2d2d2", "test") }
                            )
                        )

                    )
                    if (myLocationClicked) {
                        val ownLoc = LatLng(parseDouble(lat), parseDouble(log))
                        setOwnMarker(ownLoc)
                    }
//                    if (myLocationClicked) {
//                        etLocation!!.setText(Util.getAddress(activity, ownLoc.latitude, ownLoc.longitude))
//                        btnSearchOne!!.setText(Util.getAddress(activity, ownLoc.latitude, ownLoc.longitude))
//                    }
                    btnSearch!!.setText(
                        Util.getAddress(
                            activity,
                            midLatLng.latitude,
                            midLatLng.longitude
                        )
                    )
                    etLocationOne!!.setText(
                        Util.getAddress(
                            activity,
                            midLatLng.latitude,
                            midLatLng.longitude
                        )
                    )
                }
                mMap!!.setOnCameraMoveStartedListener {
                    mMap?.clear()
                    cabDetail?.let { setcabsInMap(it) }
                    if (myLocationClicked) {
                        val ownLoc = LatLng(parseDouble(lat), parseDouble(log))
                        setOwnMarker(ownLoc)
                    }
                    dummyMarker.visibility = View.VISIBLE

                }

                /*mMap!!.setOnCameraIdleListener(GoogleMap.OnCameraIdleListener {
                    //get latlng at the center by calling
                    mMap?.clear()
                    val midLatLng = mMap!!.getCameraPosition().target
                    mMap!!.addMarker(
                        MarkerOptions()
                            .position(midLatLng)
                            .title("pin")
                            .icon(
                                BitmapDescriptorFactory
                                    .defaultMarker(BitmapDescriptorFactory.HUE_GREEN)
                            )
                    )
                })*/


            } else {
                val ownLoc = LatLng(parseDouble(lat), parseDouble(log))
                mMap?.clear()
                cabDetail?.let { setcabsInMap(it) }
//                mMap?.moveCamera(CameraUpdateFactory.newLatLng(ownLoc))
                var pref: PreferenceManager
                pref = PreferenceManager(activity)
                setOwnMarker(LatLng(pref.locationLat.toDouble(), pref.locationLon.toDouble()))
//                etLocation!!.setText(Util.getAddress(activity, ownLoc.latitude, ownLoc.longitude))
                mMap?.addMarker(
                    MarkerOptions().position(ownLoc).icon(
                        BitmapDescriptorFactory.fromBitmap(
                            activity?.let { Util.createCustomMarker(it, "#d2d2d2", "test") }
                        )
                    )

                )
//                myMarker?.setTag("DRAGGED_LOCATION")
//                mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(ownLoc, 15f))
            }
        }
    }


    private fun setOwnMarker(ownLoc: LatLng) {
        mMap?.addMarker(
            MarkerOptions().position(ownLoc).icon(
                BitmapDescriptorFactory.fromBitmap(
                    activity?.let { Util.createCustomMarker(it) }
                )
            ).draggable(true)
        )
//        myMarker?.setTag("OWN_LOCATION")
    }

    fun onError(error: String) {
        progress.visibility = GONE
        Util.showSnackBar(mapFragment?.getView()!!, "Error " + error)
    }

    var request: Request? = null
    var cabDetail: CabDetails? = null

    private fun getCabDetails() {

        val call = RetrofitClient.getAPIInterface().getCabList("77.6476", "12.9081")
        request = RetrofitRequest(call, object : ResponseListener<CabDetails> {
            override fun onResponse(response: CabDetails, headers: Headers) {
                Log.d("LoginActivity", "response $response")
//                Util.showToast(this@LoginActivity, response.message.toString())
                cabDetail = response
                cabDetail?.let { setcabsInMap(it) }


            }

            override fun onError(status: Int, error: String) {
//                progressBar.visibility = GONE
                Log.d("LoginActivity", "onError " + error)
//                Util.showToast(this@LoginActivity, "Error")

            }

            override fun onFailure(throwable: Throwable) {
//                progressBar.visibility = GONE
                Log.d("LoginActivity", "onFailure")
//                Util.showToast(this@LoginActivity, "Failure")

            }
        })
        request!!.enqueue()
    }

    private fun setcabsInMap(response: CabDetails) {

        if (response.data != null && response.data!!.size > 0) {

            for (item in response!!.data!!) {

                var cabBitmap =
                    activity?.let { createCabMarker(it) }
                mMap?.addMarker(
                    MarkerOptions().position(
                        LatLng(
                            item.latitude!!.toDouble(),
                            item.longitude!!.toDouble()
                        )
                    ).icon(
                        BitmapDescriptorFactory.fromBitmap(cabBitmap)
                    )

                )
            }

        } else {
            activity?.let { Util.showToast(it, "Got empty data") }
        }
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        googleMap.mapType = GoogleMap.MAP_TYPE_NORMAL
        googleMap.uiSettings.isZoomControlsEnabled = true
//        googleMap.isMyLocationEnabled = true
        (activity as (HomeActivity)).getLocation()
        mMap?.setOnMarkerClickListener(this)
        mMap?.setOnMapLongClickListener(this)
//        mMap?.setOnMarkerDragListener(this)

    }

//    override fun onMarkerDragEnd(p0: Marker?) {
//        //TODO: implement later
//        Log.d(TAG, "onMarkerDragEnd " + p0!!.position.latitude + " - " + p0!!.position.longitude)
////        onLocationReceived("" + p0!!.position.latitude, "" + p0!!.position.longitude, false);
////        val ownLoc = LatLng(p0!!.position.latitude, p0!!.position.longitude)
////        etLocation!!.setText(Util.getAddress(activity, ownLoc.latitude, ownLoc.longitude))
//
//    }
//
//    override fun onMarkerDragStart(p0: Marker?) {
//
//        Log.d(TAG, "onMarkerDragStart " + p0!!.position.latitude + " - " + p0!!.position.longitude)
////        etLocation!!.setText(Util.getAddress(activity, p0!!.position.latitude, p0!!.position.longitude))
//    }
//
//    override fun onMarkerDrag(p0: Marker?) {
//        Log.d(TAG, "onMarkerDrag " + p0!!.position.latitude + " - " + p0!!.position.longitude)
////        onLocationReceived("" + p0!!.position.latitude, "" + p0!!.position.longitude, false);
//
//    }

    override fun onMarkerClick(marker: Marker): Boolean {

        Log.d(
            TAG,
            "onMarkerClick " + marker!!.position.latitude + " - " + marker!!.position.longitude
        )
        if (marker.tag != "OWN_LOCATION") {
            val latLng = LatLng(marker!!.position.latitude, marker!!.position.longitude)
            mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))
        }
        return false
    }

    override fun onMapLongClick(p0: LatLng?) {
        Log.d(TAG, "onMapLongClick " + p0!!.latitude + " - " + p0!!.longitude)
//        onLocationReceived("" + p0!!.latitude, "" + p0!!.longitude, false);

    }

}

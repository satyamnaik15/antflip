package antflip.home

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class CabDetails {

    @SerializedName("code")
    @Expose
    var code: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("data")
    @Expose
    var data: List<Datum>? = null

    inner class Datum {
        @SerializedName("KM")
        @Expose
        var kM: Int? = null
        @SerializedName("driver_id")
        @Expose
        var driverId: Int? = null
        @SerializedName("driver_first_name")
        @Expose
        var driverFirstName: String? = null
        @SerializedName("driver_last_name")
        @Expose
        var driverLastName: String? = null
        @SerializedName("driver_email")
        @Expose
        var driverEmail: String? = null
        @SerializedName("driver_contact_no")
        @Expose
        var driverContactNo: Long? = null
        @SerializedName("driver_licence")
        @Expose
        var driverLicence: String? = null
        @SerializedName("licence_expiry_date")
        @Expose
        var licenceExpiryDate: Int? = null
        @SerializedName("driver_passport_size_photo")
        @Expose
        var driverPassportSizePhoto: Any? = null
        @SerializedName("place_name")
        @Expose
        var placeName: String? = null
        @SerializedName("longitude")
        @Expose
        var longitude: Double? = null
        @SerializedName("latitude")
        @Expose
        var latitude: Double? = null
        @SerializedName("vehicle_no")
        @Expose
        var vehicleNo: String? = null
        @SerializedName("vehicle_reg_no")
        @Expose
        var vehicleRegNo: String? = null
        @SerializedName("vehicle_insurance_no")
        @Expose
        var vehicleInsuranceNo: String? = null
        @SerializedName("cab_image_url1")
        @Expose
        var cabImageUrl1: String? = null
        @SerializedName("cab_image_url2")
        @Expose
        var cabImageUrl2: String? = null
        @SerializedName("model_name")
        @Expose
        var modelName: String? = null
        @SerializedName("model_type")
        @Expose
        var modelType: String? = null
        @SerializedName("brand_name")
        @Expose
        var brandName: String? = null
        @SerializedName("color_name")
        @Expose
        var colorName: String? = null


    }
}
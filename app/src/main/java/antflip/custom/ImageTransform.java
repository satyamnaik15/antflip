package antflip.custom;

import android.view.View;
import android.widget.ImageView;

import androidx.viewpager.widget.ViewPager;
import antflip.R;


public class ImageTransform implements ViewPager.PageTransformer {

    private static final float MIN_SCALE = 0.15f;

    public void transformPage(View view, float position) {
        int pageWidth = view.getWidth();

        ImageView whole = (ImageView) view.findViewById(R.id.image);

        if (position < -1) { // [-Infinity,-1)
            // This page is way off-screen to the left.
            if (whole != null) {

                whole.setTranslationX(0);

            }

        } else if (position <= 0) { // [-1,0]
            // Use the default slide transition when moving to the left page


            doTrans(view, position, whole);


        } else if (position <= 1) { // (0,1]
            // Fade the page out.
            doTrans(view, position, whole);

        } else { // (1,+Infinity]
            // This page is way off-screen to the right.
            if (whole != null) {

                whole.setTranslationX(0);
            }
        }
    }

    private void doTrans(View view, float position, View description) {
        if (position != 0) {

            final float translationX = view.getWidth() * position;
            description.setTranslationX(-translationX);

        } else {

            description.setTranslationX(0);
        }
    }

}

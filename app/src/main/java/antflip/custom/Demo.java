package antflip.custom;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.viewpager.widget.ViewPager;
import antflip.R;


/**
 * Created by Satyam on 08/01/16.
 */
public class Demo implements ViewPager.PageTransformer {

    private static final float MIN_SCALE = 0.15f;

    public void transformPage(View view, float position) {
        int pageWidth = view.getWidth();

        ImageView whole=(ImageView)view.findViewById(R.id.student_success_logo_onboarding);
        ImageView parent_icon_onboarding=(ImageView)view.findViewById(R.id.parent_icon_onboarding);
        TextView textSplashHeader=(TextView) view.findViewById(R.id.textSplashHeader);
        TextView textSplashDetails=(TextView) view.findViewById(R.id.textSplashDetails);


        if (position < -1) { // [-Infinity,-1)
            // This page is way off-screen to the left.
            if(whole!=null) {

                whole.setTranslationX(0);
                parent_icon_onboarding.setTranslationX(0);
                textSplashHeader.setTranslationX(0);
                textSplashDetails.setTranslationX(0);

            }

        } else if (position <= 0) { // [-1,0]
            // Use the default slide transition when moving to the left page


                doTrans(view,position,whole);
            doTrans(view,position,parent_icon_onboarding);
            doTrans(view,position,textSplashHeader);
            doTrans(view,position,textSplashDetails);


        } else if (position <= 1) { // (0,1]
            // Fade the page out.
            doTrans(view,position,whole);
            doTrans(view,position,parent_icon_onboarding);
            doTrans(view,position,textSplashHeader);
            doTrans(view,position,textSplashDetails);

        } else { // (1,+Infinity]
            // This page is way off-screen to the right.
            if(whole!=null) {

                whole.setTranslationX(0);
                parent_icon_onboarding.setTranslationX(0);
                textSplashHeader.setTranslationX(0);
                textSplashDetails.setTranslationX(0);
            }
        }
    }

    private void doTrans(View view, float position, View description) {
        if (position != 0) {

            final float translationX = view.getWidth() * position;
            description.setTranslationX(-translationX);

        } else {

            description.setTranslationX(0);
        }
    }

}

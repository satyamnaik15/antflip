package antflip.custom;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.ColorRes;
import androidx.core.content.ContextCompat;
import antflip.R;


public class CircleIndicatorView extends View {

    private Context context;
    private Paint activeIndicatorPaint;
    private Paint inactiveIndicatorPaint;
    private Paint activeIndicatorPaintinnercircle;
    private int radius;
    private int size;
    private int position;
    private int indicatorsCount;

    public CircleIndicatorView(Context context) {
        super(context);
        init(context);
    }

    public CircleIndicatorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CircleIndicatorView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        this.context = context;

//        activeIndicatorPaint.setColor(ContextCompat.getColor(context, R.color.active_indicator));
        inactiveIndicatorPaint = new Paint();
//        inactiveIndicatorPaint.setColor(ContextCompat.getColor(context, R.color.inactive_indicator));
        inactiveIndicatorPaint.setAntiAlias(true);

        activeIndicatorPaint = new Paint();
        activeIndicatorPaint.setColor(ContextCompat.getColor(context, R.color.inactive_indicatorblue));

        activeIndicatorPaint.setStyle(Paint.Style.STROKE);
        activeIndicatorPaint.setStrokeWidth(4);
        activeIndicatorPaint.setAlpha(255);
        // paint.setXfermode(xfermode);
        activeIndicatorPaint.setAntiAlias(true);
        // setBackgroundColor(Color.BLACK);
        radius = getResources().getDimensionPixelSize(R.dimen.indicator_size);
        size = radius * 2;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        for (int i = 0; i < indicatorsCount; i++) {
            if(position==0) {
                activeIndicatorPaint.setColor(ContextCompat.getColor(context, R.color.inactive_indicatorblue));

                inactiveIndicatorPaint.setColor(ContextCompat.getColor(context, R.color.inactive_indicatorblue));
                if (position == i) {
                    //active stroke
                    canvas.drawCircle(radius + (size * i), radius, radius-(((float) radius / 100) * 20), activeIndicatorPaint);

                } else {
                    //inactive circles
                    canvas.drawCircle(radius + (size * i), radius, radius / 2, inactiveIndicatorPaint);
                }
            }
            else if(position==1){
                activeIndicatorPaint.setColor(ContextCompat.getColor(context, R.color.inactive_indicatorgreen));

                inactiveIndicatorPaint.setColor(ContextCompat.getColor(context, R.color.inactive_indicatorgreen));
                if (position == i) {
                    canvas.drawCircle(radius + (size * i), radius, radius-(((float) radius / 100) * 20), activeIndicatorPaint);

                } else
                    canvas.drawCircle(radius + (size * i), radius, radius / 2, inactiveIndicatorPaint);

                 }else{
                inactiveIndicatorPaint.setColor(ContextCompat.getColor(context, R.color.inactive_indicatorred));
                activeIndicatorPaint.setColor(ContextCompat.getColor(context, R.color.inactive_indicatorred));

                if (position == i) {
                    canvas.drawCircle(radius + (size * i), radius, radius-(((float) radius / 100) * 20), activeIndicatorPaint);

                } else
                    canvas.drawCircle(radius + (size * i), radius, radius / 2, inactiveIndicatorPaint);

            }
        }
        //inner active circle
//        if(position==0) {
//            inactiveIndicatorPaint.setColor(ContextCompat.getColor(context, R.color.inactive_indicatorblue));
//
//            canvas.drawCircle(radius + (size * position), radius, radius-(((float) radius / 100) * 7), inactiveIndicatorPaint);
//
//        }else if(position==1) {
//            inactiveIndicatorPaint.setColor(ContextCompat.getColor(context, R.color.inactive_indicatorgreen));
//
//            canvas.drawCircle(radius + (size * position), radius, radius - (((float) radius / 100) * 7), inactiveIndicatorPaint);
//        } else {
//            inactiveIndicatorPaint.setColor(ContextCompat.getColor(context, R.color.inactive_indicatorred));
//
//            canvas.drawCircle(radius + (size * position), radius, radius - (((float) radius / 100) * 7), inactiveIndicatorPaint);
//
//        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(measureWidth(widthMeasureSpec), measureHeight(heightMeasureSpec));
    }

    public void setCurrentPage(int position) {
        this.position = position;
        invalidate();
    }

    public void setPageIndicators() {
        this.indicatorsCount = 3;
        invalidate();
    }

    private int measureWidth(int measureSpec) {
        int result = 0;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else {
            result = size * indicatorsCount;
            if (specMode == MeasureSpec.AT_MOST) {
                result = Math.min(result, specSize);
            }
        }
        return result;
    }

    private int measureHeight(int measureSpec) {
        int result = 0;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else {
            result = 2 * radius + getPaddingTop() + getPaddingBottom();
            if (specMode == MeasureSpec.AT_MOST) {
                result = Math.min(result, specSize);
            }
        }
        return result;
    }

    public void setInactiveIndicatorColor(@ColorRes int color) {
        inactiveIndicatorPaint.setColor(ContextCompat.getColor(context, color));
        invalidate();
    }

    public void setActiveIndicatorColor(@ColorRes int color) {
        activeIndicatorPaint.setColor(ContextCompat.getColor(context, color));
        invalidate();
    }

}

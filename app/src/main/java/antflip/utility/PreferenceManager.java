package antflip.utility;

import android.content.Context;
import android.content.SharedPreferences;


public class PreferenceManager {

    private final static String FORTIS_PREFERENCES = "cyron_saloon";
    private final static String FIREBASE_TOKEN = "firebase_token";
    private final static String LOGIN_ID = "login_id";
    private final static String LOGIN_PASSWORD= "login_password";
    private final static String LOCATION_LAT = "location_lat";
    private final static String LOCATION_LON = "location_lon";


    private SharedPreferences sharedPreferences;

    private PreferenceManager() {

    }

    public PreferenceManager(Context context) {
        this();
        sharedPreferences = context.getApplicationContext().getSharedPreferences(FORTIS_PREFERENCES, Context.MODE_PRIVATE);
    }


    public void setFirebaseToken(String token) {
        sharedPreferences.edit().putString(FIREBASE_TOKEN, token).apply();
    }

    public String getFirebaseToken() {
        String token = sharedPreferences.getString(FIREBASE_TOKEN, "");
        return token;
    }


    public void setLocation(String lat, String lon) {
        sharedPreferences.edit().putString(LOCATION_LAT, lat).apply();
        sharedPreferences.edit().putString(LOCATION_LON, lon).apply();

    }

    public String getLocationLat() {
        return sharedPreferences.getString(LOCATION_LAT, "");
    }

    public String getLocationLon() {
        return sharedPreferences.getString(LOCATION_LON, "");
    }

    public String getLoginId() {
        return sharedPreferences.getString(LOGIN_ID, "");
    }

    public void setLoginId(String id) {
        sharedPreferences.edit().putString(LOGIN_ID, id).apply();
    }
    public void setLoginPassword(String password) {
        sharedPreferences.edit().putString(LOGIN_PASSWORD, password).apply();
    }
}

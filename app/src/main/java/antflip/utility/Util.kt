package antflip.utility

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.location.Geocoder
import android.net.Uri
import android.provider.Settings
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AlertDialog
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.app.ActivityCompat
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.FragmentActivity
import antflip.R
import antflip.utility.Constant.Companion.ACCESS_FINE_LOCATION_PERMISSION
import antflip.utility.Constant.Companion.MY_PERMISSIONS_REQUEST_LOCATION
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*

class Util {
    companion object {
        val MY_PREF = "XpsPref"
        val ONBOARDING_FLAG = "onboardingFlag"


        fun openWithChrome(context: Context, url: String) {
            val builder = CustomTabsIntent.Builder()
            if (context != null) {
                builder.setToolbarColor(
                    ResourcesCompat.getColor(
                        context.resources,
                        R.color.colorPrimary,
                        null
                    )
                )
            }
            val customTabsIntent = builder.build()
            customTabsIntent.launchUrl(context, Uri.parse(url))
        }

        fun hideKeyboardFrom(view: View) {
            val imm =
                view.context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }

        fun toast(activity: Activity, str: String) {
            Toast.makeText(activity, str, Toast.LENGTH_SHORT).show()
        }

        fun isLocationPermitted(activity: Activity): Boolean {
            val result =
                ActivityCompat.checkSelfPermission(activity, ACCESS_FINE_LOCATION_PERMISSION)
            return result == PackageManager.PERMISSION_GRANTED
        }

        fun showTextInputError(textInputLayout: TextInputLayout, message: String) {
            textInputLayout.editText!!.requestFocus()
            textInputLayout.isErrorEnabled = true
            textInputLayout.error = message
        }

        fun showSnackBar(view: View, message: String) {
            val snackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT)
            snackbar.show()
        }

        fun showToast(activity: Activity, message: String) {
            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
        }

        fun getDeviceId(context: Context): String {
            val device_uuid =
                Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
            return device_uuid ?: ""
        }

        fun showAlert(
            context: Context,
            cancelable: Boolean,
            messageResId: String,
            positiveMessageId: String,
            onClickListener: DialogInterface.OnClickListener
        ) {
            val builder = AlertDialog.Builder(context)
            builder.setTitle(context.getString(R.string.app_name))
            builder.setMessage(messageResId)
            builder.setCancelable(cancelable)
            builder.setPositiveButton(positiveMessageId, onClickListener)
            builder.create().show()
        }

        fun openAppSettingScreen(context: Activity) {
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            val uri = Uri.fromParts("package", context.packageName, null)
            intent.data = uri
            context.startActivityForResult(intent, MY_PERMISSIONS_REQUEST_LOCATION)
        }


        fun convertPixelsToDp(px: Float, context: Context): Float {
            return px / (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
        }

        fun createCabMarker(
            context: Context
        ): Bitmap {

            val marker =
                (context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
                    R.layout.custom_cab_layout,
                    null
                )

            val displayMetrics = DisplayMetrics()
            (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
            marker.setLayoutParams(ViewGroup.LayoutParams(40, ViewGroup.LayoutParams.WRAP_CONTENT))
            marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels)
            marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels)
            marker.buildDrawingCache()
            val bitmap = Bitmap.createBitmap(
                marker.getMeasuredWidth(),
                marker.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(bitmap)
            marker.draw(canvas)

            return bitmap
        }

        fun createCustomMarkerInsideCircle(
            context: Context, @DrawableRes resource: Int,
            _name: String
        ): Bitmap {

            val marker =
                (context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
                    R.layout.custom_marker_layout,
                    null
                )

            val markerImage = marker.findViewById(R.id.user_dp) as CircleImageView
            markerImage.setImageResource(resource)

            val displayMetrics = DisplayMetrics()
            (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
            marker.setLayoutParams(ViewGroup.LayoutParams(40, ViewGroup.LayoutParams.WRAP_CONTENT))
            marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels)
            marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels)
            marker.buildDrawingCache()
            val bitmap = Bitmap.createBitmap(
                marker.getMeasuredWidth(),
                marker.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(bitmap)
            marker.draw(canvas)

            return bitmap
        }

        fun createCustomMarker(context: Context): Bitmap {

            val marker =
                (context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
                    R.layout.own_location,
                    null
                )

            val displayMetrics = DisplayMetrics()
            (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
            marker.setLayoutParams(
                ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
            )
            marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels)
            marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels)
            marker.buildDrawingCache()
            val bitmap =
                Bitmap.createBitmap(
                    marker.getMeasuredWidth(),
                    marker.getMeasuredHeight(),
                    Bitmap.Config.ARGB_8888
                )
            val canvas = Canvas(bitmap)
            marker.draw(canvas)

            return bitmap
        }

        fun createCustomMarker(context: Context, color: String, _name: String): Bitmap {

            val marker =
                (context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
                    R.layout.custom_marker_layout,
                    null
                )

            val markerImage = marker.findViewById(R.id.user_dp) as CircleImageView
            markerImage.setBackgroundColor(Color.parseColor(color))

            val displayMetrics = DisplayMetrics()
            (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
            marker.setLayoutParams(
                ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
            )
            marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels)
            marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels)
            marker.buildDrawingCache()
            val bitmap =
                Bitmap.createBitmap(
                    marker.getMeasuredWidth(),
                    marker.getMeasuredHeight(),
                    Bitmap.Config.ARGB_8888
                )
            val canvas = Canvas(bitmap)
            marker.draw(canvas)

            return bitmap
        }

        fun getAddress(activity: FragmentActivity?, latitude: Double, longitude: Double): String {
            var geocoder = Geocoder(activity, Locale.getDefault());

            var addresses = geocoder.getFromLocation(
                latitude,
                longitude,
                1
            ); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            if (addresses.size <= 0)
                return ""
            var address = addresses.get(0)
                .getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            var city = addresses.get(0).getLocality();
            var state = addresses.get(0).getAdminArea();
            var country = addresses.get(0).getCountryName();
            var postalCode = addresses.get(0).getPostalCode();
            var knownName = addresses.get(0).getFeatureName();

            return address;

        }
    }
}
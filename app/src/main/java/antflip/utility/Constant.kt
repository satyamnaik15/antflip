package antflip.utility

import android.Manifest

class Constant {
    companion object {
        const val HOME_TITLE = "SXPS"
        const val GALLARY_TITLE = "Gallery"
        const val GALLARY_DETAILS = "Gallery_Details"
        const val NOTICE_TITLE = "Notice"
        const val NOTIFICATION_TITLE = "Notification"
        const val CONTACT_US_TITLE = "Contact Us"
        const val AUTOCOMPLETE_REQUEST_CODE  = 1

        const val BUNDELKEY_GALLARY_CLICKED_ITEM = "Gallary_data"
        const val BUNDELKEY_GALLARY_ITEM_POS = "BUNDELKEY_GALLARY_ITEM_POS"
        const val PACKAGE_NAME = "cyron.antflip"

        const val GPS_REQUEST_CODE = 1000
        const val MY_PERMISSIONS_REQUEST_LOCATION = 99
        const val ACTIVITY_EXTRA = PACKAGE_NAME + ".ACTIVITY_EXTRA"
        const val LATEST_LATITUDE = PACKAGE_NAME + ".Latitude"
        const val LATEST_LONGITUDE = PACKAGE_NAME + ".Longitude"
        const val ACCESS_FINE_LOCATION_PERMISSION = Manifest.permission.ACCESS_FINE_LOCATION


    }

}
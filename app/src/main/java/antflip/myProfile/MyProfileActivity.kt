package antflip.myProfile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import antflip.R

class MyProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_profile)
    }
}

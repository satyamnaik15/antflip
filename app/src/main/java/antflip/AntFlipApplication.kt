package antflip

import android.app.Application
import android.content.Context
import android.util.Log
import antflip.retrofit.RetrofitClient
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult


class AntFlipApplication : Application() {

    init {
        instance = this
    }

    override fun onCreate() {
        super.onCreate()

        val context: Context = applicationContext()
        RetrofitClient.create(cacheDir)
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(object : OnCompleteListener<InstanceIdResult> {
                override fun onComplete(task: Task<InstanceIdResult>) {
                    if (!task.isSuccessful()) {
                        Log.d("NMApplication", "getInstanceId failed", task.getException())
                        return
                    }

                    // Get new Instance ID token
                    val token = task.getResult()!!.getToken()
                    Log.d("NMApplication", "token $token")

                }
            })
    }

    companion object {
        private var instance: AntFlipApplication? = null

        fun applicationContext(): Context {
            return instance!!.applicationContext
        }

        /*  fun getApiService(): ApiInterface {

              if (apiService == null) {
                  val logging = HttpLoggingInterceptor()
                  logging.level = HttpLoggingInterceptor.Level.BODY
                  val httpClient = RetrofitUitl.getUnsafeOkHttpClient()
                  httpClient.addInterceptor(logging)

                  val retrofit = Retrofit.Builder()
                          .baseUrl(ApiInterface.BASE_URL)
                          .addConverterFactory(GsonConverterFactory.create())
                          .client(httpClient.build())
                          .build()
                  apiService = retrofit.create(ApiInterface::class.java)
              }
              return apiService as ApiInterface
          }*/
    }


}
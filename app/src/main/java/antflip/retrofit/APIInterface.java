package antflip.retrofit;


import antflip.account.SigninModel;
import antflip.account.SignupModel;
import antflip.home.CabDetails;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface APIInterface {


    @FormUrlEncoded
    @POST("signin")
    Call<SigninModel.Example> signIn(@Field("email") String email, @Field("password") String password, @Field("type") String type);

    @FormUrlEncoded
    @POST("signup")
    Call<SignupModel.Example> signUp(@Field("email") String email, @Field("password") String password,
                                     @Field("type") String type, @Field("first_name") String firstName
            , @Field("last_name") String lastName, @Field("phone") String phone,
                                     @Field("gender") String gender);

    @GET("finddriver/{lat}/{lon}")
    Call<CabDetails> getCabList(@Path("lat") String lat, @Path("lon") String lon);
    /* @POST("5db6a36b0fa3d408cc3dfc99/turnLight")
     Call<BulbStatusResponseModel> updateBulbStatus(@Body BulbStatusUpdate statusUpdate);

     @POST("login")
     Call<SignInResponseModel> signIn(@Body SigninModel signinModel);

 */
    interface Header {
        String AUTHORIZATION = "Authorization";
        String TIMEZONE = "Timezone";

    }

//    @Multipart
//    @POST("patients/register")
//    Call<Response> register(@Part MultipartBody.Part file, @PartMap() Map<String, RequestBody> partMap);


//    @GET("configurations")
//    Call<Configuration> downloadConfiguration();

//    @FormUrlEncoded
//    @POST("patients/get-otp")
//    Call<Response> requestOTP(@Field("uid") String mobileNo);

//    @POST
//    Call<Response> saveMedicineTrackerInfo(@Url String url, @Body SaveMedicineReadingRequestModel medicine);

//    @GET("5db6a36b0fa3d408cc3dfc99/getLightStatus")
//    Call<BulbStatusResponseModel> getBulbStatus();
}

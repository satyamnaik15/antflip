package antflip

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.Toolbar
import antflip.account.LoginActivity
import antflip.history.HistoryActivity
import antflip.home.HomeFragment
import antflip.myProfile.MyProfileActivity
import antflip.utility.Constant.Companion.AUTOCOMPLETE_REQUEST_CODE
import antflip.utility.Constant.Companion.HOME_TITLE
import antflip.utility.FragmentHelper
import antflip.utility.FragmentHelper.getFragment
import antflip.utility.PreferenceManager
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.model.TypeFilter
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import java.util.*


class HomeActivity : LocationBaseClass() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        setHome()
        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), getString(R.string.google_maps_key), Locale.US);
        }

        var pref: PreferenceManager
        pref = PreferenceManager(this)
        pref.setLoginId("TEST")

    }

    fun setHome() {
        FragmentHelper.clearBackStack(this)
        val fragment = HomeFragment()
        FragmentHelper.replaceFragment(this@HomeActivity, R.id.home_container, fragment, false, HOME_TITLE)

    }

    override fun onLocationReceived(lat: String, log: String) {
        var pref: PreferenceManager
        pref = PreferenceManager(this)
        pref.setLocation(lat, log)
        (getFragment(this@HomeActivity, HOME_TITLE) as HomeFragment).onLocationReceived(lat, log, true)
    }

    override fun onError(error: String) {
        (getFragment(this@HomeActivity, HOME_TITLE) as HomeFragment).onError(error)
    }

    fun placeSearch() {
        // Start the autocomplete intent.
        val fields = Arrays.asList(Place.Field.ID, Place.Field.NAME)
        val intent = Autocomplete.IntentBuilder(
            AutocompleteActivityMode.OVERLAY, fields
        )
            .setTypeFilter(TypeFilter.ADDRESS)
            .build(this)
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode === AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode === Activity.RESULT_OK) {
                val place = Autocomplete.getPlaceFromIntent(data!!)
                Log.i("TAG", "Place: " + place)

                if (place == null)
                    return

                Log.i("TAG", "Place: " + place.getName() + ", " + place.getId())
                (getFragment(
                    this@HomeActivity,
                    HOME_TITLE
                ) as HomeFragment).onLocationReceived(
                    "" + place.latLng!!.latitude,
                    "" + place.latLng!!.longitude,
                    false
                )
            } else if (resultCode === AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                val status = Autocomplete.getStatusFromIntent(data!!)
                Log.i("TAG", status.getStatusMessage())
            } else if (resultCode === Activity.RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu to use in the action bar
        val inflater = menuInflater
        inflater.inflate(R.menu.toolbar_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle presses on the action bar menu items
        when (item.itemId) {
            R.id.profile -> {
                val intent = Intent(this@HomeActivity, MyProfileActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.history -> {
                val intent = Intent(this@HomeActivity, HistoryActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.logout -> {
                var pref: PreferenceManager = PreferenceManager(this)
                pref.setLoginId("")
                val intent = Intent(this@HomeActivity, LoginActivity::class.java)
                startActivity(intent)
                finish()
                return true
            }

        }
        return super.onOptionsItemSelected(item)
    }

}

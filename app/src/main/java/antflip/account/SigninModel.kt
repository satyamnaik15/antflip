package antflip.account

import com.google.gson.annotations.SerializedName

class SigninModel {

    inner class Data {

        @SerializedName("id")
        var id: Int? = null
        @SerializedName("first_name")
        var firstName: String? = null
        @SerializedName("last_name")
        var lastName: String? = null
        @SerializedName("email")
        var email: String? = null
        @SerializedName("phone")
        var phone: Int? = null
        @SerializedName("password")
        var password: String? = null
        @SerializedName("status")
        var status: Int? = null
        @SerializedName("type")
        var type: String? = null
        @SerializedName("gender")
        var gender: String? = null
        @SerializedName("remember_token")
        var rememberToken: Any? = null
        @SerializedName("created_at")
        var createdAt: String? = null
        @SerializedName("updated_at")
        var updatedAt: String? = null

    }

    inner class Example {

        @SerializedName("code")
        var code: Int? = null
        @SerializedName("message")
        var message: String? = null
        @SerializedName("data")
        var data: Data? = null

    }
}

package antflip.account

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import antflip.HomeActivity
import antflip.R
import antflip.callBacks.Request
import antflip.callBacks.ResponseListener
import antflip.retrofit.RetrofitClient
import antflip.retrofit.RetrofitRequest
import antflip.utility.PreferenceManager
import antflip.utility.Util
import okhttp3.Headers

class SignupActivity : AppCompatActivity() {
    private var request: Request? = null
    var gender: String = "male"
    var type: String = "owner"
    lateinit var progressBar: ProgressBar


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        val btnSignup: Button = findViewById(R.id.btn_signup) as Button
        val userName: EditText = findViewById(R.id.input_user_name)
        val emailId: EditText = findViewById(R.id.input_user_email)
        val password: EditText = findViewById(R.id.input_password)
        val repassword: EditText = findViewById(R.id.input_re_password)
        val lastName: EditText = findViewById(R.id.input_user_last)
        val phNo: EditText = findViewById(R.id.input_user_phone)
        val spinner: Spinner = findViewById(R.id.gender)
        progressBar = findViewById(R.id.progressBar)
        val type: Spinner = findViewById(R.id.type)

        progressBar.visibility = View.GONE
        btnSignup.setOnClickListener {


            if (userName.text.toString().length == 0)
                userName.setError("Cannot be empty")
            else if (emailId.text.toString().length == 0)
                emailId.setError("Cannot be empty")
            else if (lastName.text.toString().length == 0)
                lastName.setError("Cannot be empty")
            else if (phNo.text.toString().length == 0)
                phNo.setError("Cannot be empty")
            else if (password.text.toString().length == 0)
                password.setError("Cannot be empty")
            else if (repassword.text.toString().length == 0)
                repassword.setError("Cannot be empty")
            else if (!repassword.text.toString().equals(password.text.toString()))
                repassword.setError("Password miss matched")
            else {
//                val intent = Intent(this, HomeActivity::class.java)
//                startActivity(intent)
//                finish()

                singup(
                    userName.text.toString(),
                    emailId.text.toString(),
                    password.text.toString(),
                    lastName.text.toString(),
                    phNo.text.toString()
                )
            }
        }

        setGender(spinner)
        setType(type)
    }

    private fun setType(spinner: Spinner) {

        val colors = arrayOf("owner")

        // Initializing an ArrayAdapter
        val adapter = ArrayAdapter(
            this, // Context
            android.R.layout.simple_spinner_item, // Layout
            colors // Array
        )

        // Set the drop down view resource
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line)

        // Finally, data bind the spinner object with dapter
        spinner.adapter = adapter;

        // Set an on item selected listener for spinner object
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                // Display the selected item text on text view
                type = "${parent.getItemAtPosition(position).toString()}"
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Another interface callback
            }
        }
    }

    private fun setGender(spinner: Spinner) {

        val colors = arrayOf("male", "female")

        // Initializing an ArrayAdapter
        val adapter = ArrayAdapter(
            this, // Context
            android.R.layout.simple_spinner_item, // Layout
            colors // Array
        )

        // Set the drop down view resource
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line)

        // Finally, data bind the spinner object with dapter
        spinner.adapter = adapter;

        // Set an on item selected listener for spinner object
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                // Display the selected item text on text view
                gender = "${parent.getItemAtPosition(position).toString()}"
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Another interface callback
            }
        }
    }

    private fun singup(
        firstName: String,
        email: String,
        password: String,
        lastName: String,
        phNo: String
    ) {
        progressBar.visibility = View.VISIBLE
        val call = RetrofitClient.getAPIInterface()
            .signUp(email, password, type, firstName, lastName, phNo, gender.toString())
        request = RetrofitRequest(call, object : ResponseListener<SignupModel.Example> {
            override fun onResponse(response: SignupModel.Example, headers: Headers) {
                Log.d("SingUpActivity", "response $response")
                Util.showToast(this@SignupActivity, response.message.toString())
                progressBar.visibility = View.GONE
                if (response.code == 200) {
                    var pref: PreferenceManager
                    pref = PreferenceManager(this@SignupActivity)
                    pref.setLoginId(email)
                    pref.setLoginPassword(password)
                    val intent = Intent(this@SignupActivity, HomeActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }

            override fun onError(status: Int, error: String) {
                Log.d("SingUpActivity", "onError " + error)
                Util.showToast(this@SignupActivity, "Error")
                progressBar.visibility = View.GONE
            }

            override fun onFailure(throwable: Throwable) {
                Log.d("SingUpActivity", "onFailure ")
                Util.showToast(this@SignupActivity, "Failure")
                progressBar.visibility = View.GONE
            }
        })
        request!!.enqueue()


    }
}

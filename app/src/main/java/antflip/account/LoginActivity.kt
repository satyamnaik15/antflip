package antflip.account

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import antflip.HomeActivity
import antflip.R
import antflip.callBacks.Request
import antflip.callBacks.ResponseListener
import antflip.retrofit.RetrofitClient
import antflip.retrofit.RetrofitRequest
import antflip.utility.PreferenceManager
import antflip.utility.Util.Companion.showToast
import okhttp3.Headers


class LoginActivity : AppCompatActivity() {
    private var request: Request?=null
    lateinit var progressBar: ProgressBar


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val btnLogin: Button = findViewById(R.id.btn_login)
        val btnSignup: Button = findViewById(R.id.btn_signup)
        val passwordEt: EditText = findViewById(R.id.input_password)
        val userName: EditText = findViewById(R.id.input_user_name)
        progressBar = findViewById(R.id.progressBar2)
        progressBar.visibility = View.GONE
        btnSignup.setOnClickListener {
            val intent = Intent(this, SignupActivity::class.java)
            startActivity(intent)
            finish()
        }

        btnLogin.setOnClickListener {

            if (userName.text.toString().length == 0)
                userName.setError("Cannot be empty")
            else if (passwordEt.text.toString().length == 0)
                passwordEt.setError("Cannot be empty")
            else {
                /* val intent = Intent(this, HomeActivity::class.java)
                 startActivity(intent)
                 finish()*/
                signin(userName.text.toString(), passwordEt.text.toString())
            }
        }
        var pref: PreferenceManager
        pref = PreferenceManager(this)
        if (pref.loginId.length > 0) {
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun signin(userName: String, password: String) {
        progressBar.visibility = View.VISIBLE
        val call = RetrofitClient.getAPIInterface().signIn(userName, password, "owner")
        request = RetrofitRequest(call, object : ResponseListener<SigninModel.Example> {
            override fun onResponse(response: SigninModel.Example, headers: Headers) {
                progressBar.visibility = View.GONE
                Log.d("LoginActivity", "response $response")
                showToast(this@LoginActivity, response.message.toString())
                if (response.code == 200) {
                    val intent = Intent(this@LoginActivity, HomeActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }

            override fun onError(status: Int, error: String) {
                progressBar.visibility = View.GONE
                Log.d("LoginActivity", "onError " + error)
                showToast(this@LoginActivity, "Error")

            }

            override fun onFailure(throwable: Throwable) {
                progressBar.visibility = View.GONE
                Log.d("LoginActivity", "onFailure")
                showToast(this@LoginActivity, "Failure")

            }
        })
            request!!.enqueue()


    }


}

package antflip.history;

public class HistoryModel {
    private String name;
    private String att;
    private String details;
    private String date;


    public String getName() {
        return name;
    }

    public String getAtt() {
        return att;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}

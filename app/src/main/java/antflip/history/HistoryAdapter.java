package antflip.history;


import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import antflip.R;

import java.util.ArrayList;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {
    private ArrayList<HistoryModel> holidaysModel;
    private Activity activity;

    public HistoryAdapter(Activity activity, ArrayList<HistoryModel> holidaysModel) {
        this.holidaysModel = holidaysModel;
        this.activity = activity;
    }

    @Override
    public HistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.history_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HistoryAdapter.ViewHolder viewHolder, int i) {


        if (i == 0) {
            int margintop = 0;
            float d = activity.getResources().getDisplayMetrics().density;

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    (int) (128 * d)
            );
            margintop = (int) (8 * d);

            params.setMargins(0, margintop, 0, 0);
            viewHolder.card.setLayoutParams(params);
        }



        viewHolder.tv_name.setText(holidaysModel.get(i).getName());
        viewHolder.tv_details.setText(holidaysModel.get(i).getDetails());
        viewHolder.tv_date.setText(holidaysModel.get(i).getDate());

//        viewHolder.att.setBackground();
    }

    @Override
    public int getItemCount() {
        return holidaysModel.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name, tv_details, tv_date;

        private CardView card;

        public ViewHolder(View view) {
            super(view);

            tv_name = (TextView) view.findViewById(R.id.tv_name);
            tv_details = (TextView) view.findViewById(R.id.tv_details);
            tv_date = (TextView) view.findViewById(R.id.tv_date);
            card = (CardView) view.findViewById(R.id.card);


        }
    }

}
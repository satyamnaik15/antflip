package antflip.history

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import antflip.R
import java.util.ArrayList

class HistoryActivity : AppCompatActivity() {

    private var recyclerView: RecyclerView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)
        initViews()
    }

    private fun initViews() {

        recyclerView = findViewById(R.id.recycleview)
        recyclerView!!.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(this)
        recyclerView!!.setLayoutManager(layoutManager)
        loadJSON()
    }

    private fun loadJSON() {


        val data = ArrayList<HistoryModel>()

        val tempSyabusModel = HistoryModel()
        tempSyabusModel.setName("Total fare- Rs 120")
        tempSyabusModel.setDetails("From- HSR  banglore To- Hebal banglore")
        tempSyabusModel.setDate("24-Oct-2019")
        data.add(tempSyabusModel)

        val tempSyabusModel1 = HistoryModel()
        tempSyabusModel1.setName("Total - Rs 120")
        tempSyabusModel1.setDetails("From- Marathalli banglore To- BTM banglore")
        tempSyabusModel1.setDate("23-Oct-2019")
        data.add(tempSyabusModel1)

        val tempSyabusModel2 = HistoryModel()
        tempSyabusModel2.setName("Total fare- Rs 220")
        tempSyabusModel2.setDetails("From- HAL  banglore To- Marathalli banglore")
        tempSyabusModel2.setDate("22-Oct-2019")
        data.add(tempSyabusModel2)

        val tempSyabusModel3 = HistoryModel()
        tempSyabusModel3.setName("Total fare- Rs 50")
        tempSyabusModel3.setDetails("From- Tin factory  banglore To- Cox colony banglore")
        tempSyabusModel3.setDate("21-Oct-2019")
        data.add(tempSyabusModel3)

        val tempSyabusModel4 = HistoryModel()
        tempSyabusModel4.setName("Total fare- Rs 90")
        tempSyabusModel4.setDetails("From- Indranager banglore To- HL banglore")
        tempSyabusModel4.setDate("20-Oct-2019")
        data.add(tempSyabusModel4)


        val adapter = HistoryAdapter(this, data)
        recyclerView!!.setAdapter(adapter)

    }
}

package antflip.onboardingFragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import antflip.R;


public class OnboardingFragment2 extends Fragment {

    private View view;
    private OnBoardingActivity activity;
    private TextView headTxt, detailsTxt;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view= inflater.inflate(
                R.layout.frag_onboardingscreentwo,
                container,
                false
        );
        findViewByID();
        setTextFont();
//        listener=new Respond() {
//            @Override
//            public void getData(String str) {
//
//                Toast.makeText(act, ""+str, Toast.LENGTH_SHORT).show();
//            }
//        };
//        if(act instanceof OnBoardingActivity)
//        act.setListener(listener);

        return view;
    }
    private void setTextFont() {
        headTxt.setText("Contact your student and grade performances");
        detailsTxt.setText("There are many variations of passages of Lorem\n" +
                " Ipsum available, but the majority\n" +
                " have suffered alteration in some form.");
    }

    private void findViewByID() {
        headTxt = (TextView) view.findViewById(R.id.textSplashHeader);
        detailsTxt = (TextView) view.findViewById(R.id.textSplashDetails);

    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity= (OnBoardingActivity) context;
    }
}
